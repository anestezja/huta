angular.module('app', [
  'ui.router',
  'ngResource',
  'ngCookies',
  'angularFileUpload',
  'app.controllers',
  'app.services',
  'app.directives'
])
.config([
  '$stateProvider',
  '$locationProvider',
  '$urlRouterProvider',
  '$httpProvider',

  function($stateProvider, $locationProvider, $urlRouterProvider, $httpProvider) {
  $locationProvider.html5Mode(true);
  $httpProvider.interceptors.push('authInterceptor');
  $stateProvider
    .state('home', {
      url: '/',
      views: {
        "": {
          templateUrl: 'templates/home.html',
          controller: "NewsController"
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('post', {
      url: '/post/:id',
      views: {
        "": {
          templateUrl: 'templates/home-news.html'
        },
        "carousel": {
          templateUrl: "templates/baner.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('about', {
      url: '/about',
      views: {
        "": {
          templateUrl: 'templates/about.html',
          controller: 'AboutMainController'
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('about.tabone', {
      url: '/tabone',
      templateUrl: 'templates/about-tabone.html',
      controller: 'AboutTabOneController'
    })
    .state('about.tabtwo', {
      url: '/tabtwo',
      templateUrl: 'templates/about-tabtwo.html',
      controller: 'AboutTabTwoController'
    })
    .state('about.tabthree', {
      url: '/tabthree',
      templateUrl: 'templates/about-tabthree.html',
      controller: 'AboutTabThreeController'
    })
    .state('sightseeing', {
      url: '/sightseeing',
      views: {
        "": {
          templateUrl: 'templates/sightseeing.html'
        },
        "carousel": {
          templateUrl: "templates/baner.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('catalog', {
      url: '/catalog',
      views: {
        "": {
          templateUrl: 'templates/catalog.html'
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        },
        'subcategories@catalog': {
          templateUrl: "templates/subcategories.html"
        },
        'products@catalog': {
          templateUrl: "templates/products.html"
        }
      }
    })
    .state('contact', {
      url: '/contact',
      views: {
        "": {
          templateUrl: 'templates/contact.html',
          controller: 'ContactController'
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('product', {
      url: '/product/:id',
      views: {
        "": {
          templateUrl: 'templates/product.html',
          controller: 'productCtrl'
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      },
      params: {
        id: null
      }
    })
    .state('admin', {
      url: '/admin',
      views: {
        "": {
          templateUrl: 'templates/admin-forms.html',
          controller: 'AdminFormsController'
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      },
      params: {
        message: ''
      }
    })
    .state('loggedIn', {
      url: '/logged-in',
      views: {
        "": {
          templateUrl: 'templates/logged-in.html',
          resolve: {
            exampleObject: function() {
              return {
                title: 'Zarządzanie treściami strony',
                body: 'Lorem ipsum dolor sit amet.'
              }
            }
          },
          controller: 'AdminController'
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('update', {
      url: '/update',
      views: {
        "": {
          templateUrl: 'templates/update.html',
          resolve: {
            responseObject: function($http) {
              return $http.get('/update').then(function(response) {
                console.log(response);
                response.noError = true;
                return response;
              }, function(error) {
                console.log(error);
                error.noError = false;
                return error;
              })
            }
          },
          controller: function($http, $scope, responseObject) {
            // return $http.get('/update').then(function(response) {
            //   console.log(response);
            // }, function(error) {
            //   console.log(error);
            // });
            console.log(responseObject);
            if (responseObject.noError == true) {
              $scope.response = responseObject;
              $scope.noError = responseObject.noError;
            } else {
              $state.go('admin', {message: 'Zaloguj się aby uzyskać dostęp do panelu.'});
            }
          },
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('loggedOut', {
      url: '/logged-out',
      views: {
        "": {
          templateUrl: 'templates/logged-out.html'
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('loggedIn.sightseeing', {
      url: '/sightseeing',
      templateUrl: 'templates/cms/sightseeing1.html',
      controller: 'CMSsightseeingController'
    })
    .state('loggedIn.sightseeingArticle', {
      url: '/sightseeing/:id',
      templateUrl: 'templates/cms/sightseeing_article.html',
      params: {
        article: null
      },
      controller: 'CMSsightseeingArticleController'
    })
    .state('loggedIn.mainPage', {
      url: '/mainPage',
      //templateUrl: 'templates/cms/main_page.html',
      templateUrl: 'templates/cms/main_page_articles_list.html',
      controller: 'CMSmainPageArticlesListCtrl'
      //controller: 'CMSmainPageController'
    })
    .state('loggedIn.mainPageArticle', {
      url: '/mainPage/:id',
      params: {
        article: null
      },
      templateUrl: 'templates/cms/main_page_article.html',
      controller: 'CMSmainPageArticleCtrl'
    })

    /* -----
    // CMS ABOUT
    ----- */

    .state('loggedIn.about', {
      url: '/about',
      templateUrl: 'templates/cms/about.html',
      controller: 'CMSaboutCtrl'
    })
    .state('loggedIn.about.main', {
      url: '/main',
      templateUrl: 'templates/cms/about-main.html',
      controller: 'CMSaboutMainCtrl'
    })
    .state('loggedIn.about.tabone', {
      url: '/tabone',
      templateUrl: 'templates/cms/about-tab-one.html',
      controller: 'CMSaboutTabOneCtrl'
    })
    .state('loggedIn.about.tabtwo', {
      url: '/tabtwo',
      templateUrl: 'templates/cms/about-tab-two.html',
      controller: 'CMSaboutTabTwoCtrl'
    })
    .state('loggedIn.about.tabthree', {
      url: '/tabthree',
      templateUrl: 'templates/cms/about-tab-three.html',
      controller: 'CMSaboutTabThreeCtrl'
    })

    /* -----
    // END OF CMS ABOUT
    ----- */

    .state('loggedIn.catalog', {
      url: '/catalog',
      templateUrl: 'templates/cms/catalog.html',
      controller: 'CMScatalogCtrl'
    })
    .state('loggedIn.contact', {
      url: '/contact',
      templateUrl: 'templates/cms/contact.html',
      controller: 'CMScontactCtrl'
    })
    .state('loggedIn.product', {
      url: '/product/:id',
      templateUrl: 'templates/cms/product.html',
      controller: 'CMSproductCtrl',
      params: {
        id: null
      }
    })

    ;$urlRouterProvider.otherwise('/');
}]);
// service tworzy obiekt, wiec mozna sie do niego odwolywac bezposrednio, np
// w kontrolerze myService.chapajDzide();
// factory to funkcja, wiec trzeba stworzyc obiekt, ktory returnuje, np
// return {name: 'danuta'}
// oba definiuje nazwa i funkcja. Oba mozna injectowac.
 angular.module('app.controllers')
.controller('AboutMainController', [
  '$scope', 'AboutMainProvider',
  function($scope, AboutMainProvider) {
    AboutMainProvider.query(function(response) {
      console.log(response);
      $scope.mainText = response[0];
    }, function(error) {
      console.log(error);
    })
  }
])
 angular.module('app.controllers')
.controller('AboutTabOneController', [
  '$scope', 'AboutTabOneProvider',
  function($scope, AboutTabOneProvider) {

    AboutTabOneProvider.query(function(response) {
      console.log(response);
      $scope.tabOneText = response[0];
    }, function(error) {
      console.log(error);
    });


  }
])
 angular.module('app.controllers')
.controller('AboutTabThreeController', [
  '$scope', 'AboutTabThreeProvider',
  function($scope, AboutTabThreeProvider) {
    AboutTabThreeProvider.query(function(response) {
      $scope.events = response;
    }, function(error) {
      console.log(error);
    })
  }
])
 angular.module('app.controllers')
.controller('AboutTabTwoController', [
  '$scope', 'AboutTabTwoProvider',
  function($scope, AboutTabTwoProvider) {
    AboutTabTwoProvider.query(function(response) {
      $scope.events = response;
    }, function(error) {
      console.log(error);
    })
  }
])
 angular.module('app.controllers')
.controller('AdminFormsController', [
  '$scope',
  '$http',
  '$state',
  'AdminProvider',
  '$cookies',
  '$stateParams',
function($scope, $http, $state, AdminProvider, $cookies, $stateParams) {
  $scope.loggedOutMessage = $stateParams.message;

  $scope.signIn = function(isValid) {
    if (isValid) {
      const data = {
        email: $scope.signInForm.email,
        password: $scope.signInForm.password
      };

      $scope.signInForm.emailMessage = "";
      $scope.signInForm.passMessage = "";

      AdminProvider.save({id: '2'}, data)
      .$promise.then(function(result) {

        console.log(result);
        if(result.id == 0 && result.result == true) { // udane logowanie
          const state = 'loggedIn';
          $state.go(state);
          console.log(document.cookie);
        } else if(result.id == 0 && result.result == false) { // bledne haslo
          $scope.signInForm.passMessage = 'Błędne haslo'
        } else if(result.id == -1) { // nie ma takiego maila w bazie
          $scope.signInForm.emailMessage = result.message;
        }
      }, function(error) {
          //console.log(error);
      });

    } else
      console.log('Cos nie tak.')
    }


    $scope.signUp = function(isValid) {
      if ($scope.signUpForm.password == $scope.signUpForm.repeat && isValid) {
        const data = {
          email: $scope.signUpForm.email,
          password: $scope.signUpForm.password
        };

        AdminProvider.save({id: '1'}, data)
        .$promise.then(function(result) {
          console.log(result);
        }, function(error) {
          console.log(error);
        });

        $scope.signUpForm.email = "";
        $scope.signUpForm.password = "";
        $scope.signUpForm.repeat = "";
      } else
        console.log('Zle haslo, leszczu!');
      }


}]);
 angular.module('app.controllers')
.controller('AdminController', [
  '$scope',
  'exampleObject',
  'AdminProvider',
  '$state',
  '$stateParams',
  function($scope, exampleObject, AdminProvider, $state) {
  $scope.title = exampleObject.title;
  $scope.body = exampleObject.body;
  $scope.logout = function() {
    AdminProvider.get({id: 3}, function(result) {
      console.log(result);
      $state.go('admin', {message: 'Well done! You are succesfully logged out.'});
    }, function(error) {
      console.log(error);
      $state.go('admin');
    })
  }
  $scope.menu = [
    {
      name: 'main page contents',
      link: 'loggedIn.mainPage'
    },
    {
      name: 'about us',
      link: 'loggedIn.about'
    },
    {
      name: 'sightseeing',
      link: 'loggedIn.sightseeing'
    },
    {
      name: 'catalog',
      link: 'loggedIn.catalog'
    },
    {
      name: 'contact',
      link: 'loggedIn.contact'
    },


  ]
}]);
 angular.module('app.controllers')
.controller('BannerController', [function() {
  if(window.matchMedia('(max-width: 768px)').matches) {
    document.querySelector('.MainPageBaner').style.backgroundImage = "url(../img/nowe/slider/slider2mini-s.jpg)";
    //$scope.src = 'img/nowe/slider/slider2mini.jpg';
  } else {
    document.querySelector('.MainPageBaner').style.backgroundImage = "url(../img/nowe/slider/p19-widen-s-s.jpg)";
    //$scope.src = 'img/nowe/slider/slider2.jpg';
  }
}])
 angular.module('app.controllers')
.controller('CatalogController', [
  '$scope',
  '$http',
  'mockupMenuService',
  'catalogService',
  function($scope, $http, mockupMenuService, catalogService) {

  catalogService.query(function(response) {
    if(!response[0]) {
      console.log('brak wynikow');
    } else {
      $scope.products = response;
    }
  })

  /* --- menu i jakies glupoty --- */

  $scope.menu = mockupMenuService.getMenu();
  $scope.activeItem = [];
  $scope.activeSubItem = [];

  $scope.setMenu = function(x) {
    $scope.submenu = x.subcategories;
    $scope.activeItem = x;
    $scope.buttonText = 'kategoria: ' + x.name;
  };
  $scope.setSubmenu = function(x) {
    $scope.activeSubItem = x;
  }

  $scope.buttonText = 'kategorie';

  $scope.open = false;
  $scope.showSubcat = function() {
    if (window.matchMedia("(max-width: 768px)").matches) {
          ($scope.open === false)? $scope.open = true : $scope.open = false;
    }
  }

  /* --- koniec menu i glupot --- */

}]);
 angular.module('app.controllers')
.controller('CMSmainPageController', ['$scope', 'FileUploader', function($scope, FileUploader) {
  var uploader = $scope.uploader = new FileUploader({
    url: '/admin/mainPage/file'
  });

  uploader.onCompleteAll = function() {
      console.info('onCompleteAll');
  };

  uploader.onCompleteItem = function(fileItem, response, status, headers) {
      console.info('onCompleteItem', fileItem, response, status, headers);
  };

  uploader.onSuccessItem = function(fileItem, response, status, headers) {
      console.info('onSuccessItem', fileItem, response, status, headers);
  };

  uploader.onErrorItem = function(fileItem, response, status, headers) {
      console.info('onErrorItem', fileItem, response, status, headers);
  };
  
  uploader.onCancelItem = function(fileItem, response, status, headers) {
      console.info('onCancelItem', fileItem, response, status, headers);
  };

}])
 angular.module('app.controllers')
.controller('CMSsightseeingArticleController', [
  '$scope',
  'SightseeingProvider',
  '$stateParams',
  'SightseeingProvider',
  function($scope, SightseeingProvider, $stateParams, SightseeingProvider) {
    $scope.title = $stateParams.article.title;
    $scope.body = $stateParams.article.body;
    $scope.showMessage = false;

    $scope.save = function(isValid) {
      if(!isValid) {
        $scope.showMessage = true;
        $scope.class = 'warning';
        $scope.responseMessage = 'Przed przesłaniem artykułu wprowadź w nim zmiany lub ';
      } else if(isValid) {
        var data = {
          id: $stateParams.article._id,
          title: $scope.title,
          body: $scope.body
        }
        //console.log(data);
        SightseeingProvider.put(data, function(response) {
          $scope.showMessage = true;
          $scope.class = 'success';
          $scope.responseMessage = 'Zapisano zmiany. ';
          console.log('response: ');
          console.log(response);
        }, function(error) {
          $scope.showMessage = true;
          $scope.class = 'danger';
          $scope.responseMessage = 'Wystąpił błąd. ';
          console.log(error);
        })
      }
    }
}])
 angular.module('app.controllers')
.controller('CMSaboutMainCtrl', [
  '$scope', 'AboutMainProvider',
  function($scope, AboutMainProvider) {

    AboutMainProvider.query(function(response) {
      console.log(response);
      if(!response[0]) {
        $scope.mainText = {};
        $scope.visibility = true;
      } else {
        $scope.mainText = response[0];
        $scope.visibility = false;
      }

    }, function(error) {
      console.log(error);
    })

    $scope.save = function(valid) {

        let data = {
          text: $scope.mainText.text
        };

        AboutMainProvider.save(data)
        .$promise.then(function(result) {
          console.log(result);
        }, function(error) {
          console.log(error);
        })
    }

    $scope.update = function(valid, id) {
      console.log('id: ');
      console.log(id);
      if(valid) {
        let data = {
          id: id,
          text: $scope.mainText.text
        }

        AboutMainProvider.put(data)
        .$promise.then(function(result) {
          console.log(result);
        }, function(error) {
          console.log(error);
        })
      }
    };


  }
])
 angular.module('app.controllers')
.controller('CMSaboutTabOneCtrl', [
  '$scope', 'AboutTabOneProvider',
  function($scope, AboutTabOneProvider) {

    AboutTabOneProvider.query(function(response) {
      if(!response[0]) {
        $scope.tabOneText = {};
        $scope.visibility = true;
      } else {
        $scope.tabOneText = response[0];
        $scope.visibility = false;
      }

    }, function(error) {
      console.log(error);
    });

    $scope.save = function(valid) {
      if(valid) {
        let data = {
          text: $scope.tabOneText.text
        };

        AboutTabOneProvider.save(data)
        .$promise.then(function(result) {
          console.log(result);
        }, function(error) {
          console.log(error);
        })
      }
    };

    $scope.update = function(valid, id) {
      console.log('id: ');
      console.log(id);
      if(valid) {
        let data = {
          id: id,
          text: $scope.tabOneText.text
        }

        AboutTabOneProvider.put(data)
        .$promise.then(function(result) {
          console.log(result);
        }, function(error) {
          console.log(error);
        })
      }
    };

  }
])
 angular.module('app.controllers')
.controller('CMSaboutTabThreeCtrl', [
  '$scope', 'AboutTabTwoProvider',
  function($scope, AboutTabTwoProvider) {

  }

])
 
angular.module('app.controllers')
.controller('CMSaboutTabTwoCtrl', [
  '$scope', 'AboutTabTwoProvider',
  function($scope, AboutTabTwoProvider) {

    AboutTabTwoProvider.query(function(response) {
      $scope.events = response;
    }, function(error) {
      console.log(error);
    });

    $scope.send = function(valid) {
      if(valid) {
        let data = {
          date: new Date($scope.event.year, $scope.event.month - 1, $scope.event.date),
          body: $scope.event.body
        };
        console.log(data);
        $scope.event = {};
        AboutTabTwoProvider.save(data)
        .$promise.then(function(result) {
          console.log(result);
        }, function(error) {
          console.log(error);
        })
      }
    };


  // days array
  $scope.daysArray = [];
  for(var i = 1; i < 32; i++) {
    $scope.daysArray.push(i);
  };

  // months array
  $scope.monthsArray = [];
  for(var j = 1; j < 13; j++) {
    $scope.monthsArray.push(j);
  }

  $scope.delete = function(id) {

  };

  $scope.edit = function(event) {

  }

}])

// new Date(year, month, date, hours, minutes, seconds, ms)
// year with 4 digits
// months start with
// if 'date' absent, 1 assumed
// if hours / minutes / seconds / ms absent, 0 assumed
 angular.module('app.controllers')
.controller('CMSaboutCtrl', [
  '$scope', 'AboutTabTwoProvider',
  function($scope, AboutTabTwoProvider) {

  }

])
 angular.module('app.controllers')
.controller('CMScatalogCtrl', [
  '$scope', 'catalogService',
  function($scope, catalogService) {

  catalogService.query(function(response) {
    if(!response[0]) {
      console.log('brak wpisow');
    } else {
      console.log(response);
      $scope.products = response;
    }
  }, function(error) {
    console.log(error);
  });


  $scope.submit = function(valid) {
    if(valid) {
      let data = $scope.item;
      console.log(data);
      catalogService.save(data, function(response) {
        console.log(response);
        $scope.item = {}
      });
    }
  }

  }
])
 angular.module('app.controllers')
.controller('CMScontactCtrl', [
  '$scope', 'ContactProvider',
  function($scope, ContactProvider) {
    ContactProvider.query(function(response) {
      $scope.contacts = response;
      console.log(response);
    }, function(error) {
      console.log(error);
    })

    $scope.send = function(valid) {
      if(valid) {
        let data = {
          title: $scope.addContact.title,
          body: $scope.addContact.body
        }
        $scope.addContact = {};
        ContactProvider.save(data)
        .$promise.then(function(result) {
          console.log(result);
        }, function(error) {
          console.log(error);
        })
      }
    }

    $scope.edit = function(index) {
      return $scope.editable = index;
    }

    $scope.saveChanges = function(contact) {
      // jeszcze walidacja, po to byla nazwa formularza, kurwa
      // reczna walidacja
      let data = {
        id: contact._id,
        title: contact.title,
        body: contact.body
      };
      let valid = {};
      if(valid) {
        console.log('chuj chuj');
        ContactProvider.put(data, function(response) {
          console.log(response);
        }, function(error) {
          console.log(error);
        })
      }
    }

    // TO DO: przypomnienie o zapisaniu zmian
    // ale to moze dopiero przy wysiwyg'u
    // $scope.savingReminder = function(form) {
    //   console.log(form);
    // }

    $scope.remove = function(id) {
      let data = {
        id: id
      };
      ContactProvider.delete(data, function(response) {
        console.log(response);
      }, function(error) {
        console.log(error);
      })
    }

    $scope.cancel = function() {
      $scope.editable = null;
    }
  }
])
 angular.module('app')
.controller('CMSmainPageArticleCtrl', ['$scope', '$stateParams', function($scope, $stateParams) {
  $scope.title = $stateParams.article.title;
  $scope.body = $stateParams.article.body;
  $scope.image = $stateParams.article.image;
  $scope.showMessage = false;
  $scope.class = 'success';
}])
 angular.module('app.controllers')
.controller('CMSmainPageArticlesListCtrl', [
  '$scope',
  'MainPageProvider',
  function($scope, MainPageProvider) {
    MainPageProvider.query(function(response) {
      console.log(response);
      $scope.articles = response;
    }, function(error) {
      console.log(error)
    })

    $scope.send = function(isVaild) {
      var data = {
        title: $scope.mainPageArticle.title,
        body: $scope.mainPageArticle.body,
        image: $scope.mainPageArticle.image
      };
      console.log(data);
      MainPageProvider.save(data, function(response) {
        $scope.data = {};
        $scope.responseMessage = 'Well done! Article saved.';
        $scope.class = 'success';
      }, function(error) {
        console.log(error);
        $scope.responseMessage = 'Oops! Something went wrong.';
        $scope.class = 'danger';
      })
      $scope.mainPageArticle = {};
    };
    $scope.launchModal = function(article) {
      console.log('chuj');
    }
}])
 angular.module('app.controllers')
.controller('CMSproductCtrl', [
  '$scope', '$stateParams', 'productService',
  function($scope, $stateParams, productService) {
    productService.get({id: $stateParams.id}, function(response) {
      if(!response) {
        console.log('brak wpisu');
      } else {
        $scope.product = response;
        console.log(response);
      }
    });


    $scope.editionMode = false;

    $scope.edit = function() {
      return $scope.editionMode = true;
    }

    $scope.cancel = function() {
      return $scope.editionMode = false;
    }

    $scope.save = function(valid) {
      if(valid) {
        let data = $scope.product;

        productService.put(data, function(response) {
          if(!response) {
            console.log('blad');
          } else {
            console.log('zajebongo');
          }
        })
      }
    };

    $scope.delete = function() {
      productService.delete({id: $scope.product._id}, function(response) {
        if(!response) {
          console.log('blad');
        } else {
          console.log('zajebongo');
        }
      })
    }


  }
])
 angular.module('app.controllers')
.controller('ContactController', [
  '$scope', 'ContactProvider',
  function($scope, ContactProvider) {
    ContactProvider.query(function(response) {
      $scope.contacts = response;
      console.log('dupa');
      console.log(response);
    }, function(error) {
      console.log(error);
    })
  }
])
 

angular.module('app.controllers')
// .controller('ContactController', function($scope, $http) {
//
//   $scope.sendEmail = function() {
//     var emailObject = {
//                         name: $scope.name,
//                         email: $scope.email,
//                         title: $scope.title,
//                         body: $scope.body
//     };
//     $http.post("/send", JSON.stringify(emailObject), {headers: {"Content-type": "application/json"}})
//     .then(function(res) { //sukces
//       $scope.message = res.data.msg;
//     }, function(res) { // error
//       $scope.message = res.data.msg;
//     });
//     $scope.name = "";
//     $scope.email = "";
//     $scope.title = "";
//     $scope.body = "";
//   };
// })
// .controller('NewController', function($scope, $http, $stateParams, PlaceholderService) {
//   var user_id = $stateParams.id;
//   PlaceholderService.query({'id': user_id}).$promise.then(function(results) { //tu byl TestService
//     $scope.data = results[0];
//     //console.log(value);
//   })



  // function funkcja() {
  //   return TestService.query({'id': user_id})
  //   .$promise.then(function (data) {
  //     return data;
  //   });
  // }
  //$scope.dupa = TestService.query({'id': user_id});
//



  //console.log('aaa', user_id);
  //var object = { data: 'data' };
  // $http.get('/post', JSON.stringify(object))
  // .then(function(response) {
  //   $scope.dupa = response.data;
  //   console.log("sukces!");
  // });
  //})

.controller('masonryController', ['$scope', '$http', function($scope, $http) {
  // var url1 = 'https://jsonplaceholder.typicode.com/posts';
  // $http.get(url1)
  //   .then(function (data) {
  //     console.log(data);
  //     $scope.data = data.data;
  //     masonry();
  //     //return resp.json();
  //   });


  
  //   .then(function (data) {
  //     console.log(data);
  //     $scope.data = data;
  //     masonry();
  // }

//);

}]);
 angular.module('app.controllers')
.controller('NewsController', [
  '$scope',
  '$http',
  '$stateParams',
  'TestService',
  'MainPageProvider',
  function($scope, $http, $stateParams, TestService, MainPageProvider) {
    MainPageProvider.query(function(response) {
      if(!response[0]) {
        console.log('brak wpisow');
      } else {
        $scope.articles = response;

        //masonry();
      }
    })

  //console.log($stateParams);


  //$scope.users = TestService.query();

  // $http.get('/news').then(function(response) {
  //   $scope.dupa = response.data;
  //   console.log(response.data);
  // }, function(response) {
  //   var status = response.status;
  //   console.log(status);
  // });

  $scope.sendNews = function() {
    var dataObject = {
                      title: $scope.title,
                      body: $scope.body
    };
    console.log("Sending object dataObject through angular $http:");
    console.log(dataObject);

    $http.post("/new", JSON.stringify(dataObject), {headers: {"Content-type": "application/json"}})
    .then(function(res) {
      //sukces
    });
  };
}]);
 angular.module('app.controllers')
.controller('MenuController', ['$scope', '$http', 'MenuService', function($scope, $http, MenuService) {
  // $http.get('/').then(function(response) {
  //
  // });
  // $http.get('/home').then(function(response) {
  //   $scope.dupa = response.data;
  //   //console.log("sukces!");
  // });
  $scope.dupa = MenuService.getMenu();

}]);
 angular.module('app.controllers')
.controller('productCtrl', [
  '$scope', 'productService', '$stateParams',
  function($scope, productService, $stateParams) {
    productService.get({id: $stateParams.id}, function(result) {
      if(result) {
        $scope.product = result;
      } else {
        console.log('brak wpisow');
      }
    });

  }
])
 angular.module('app.controllers')
.controller('CMSsightseeingController', [
  '$scope',
  'SightseeingProvider',
  '$http',
  function($scope, SightseeingProvider, $http) {
  $scope.responseMessage = '';

  SightseeingProvider.query(function(response) {
    console.log(response)
    $scope.articles = response;
  }, function(error) {
    console.log(err);
  })

  $scope.launchModal = function(article) {
    $scope.title = article.title;
    $scope.articleToDelete = article;
  }

  $scope.delete = function(article) {
    data = {
      id: article._id
    };
    SightseeingProvider.delete(data, function(response) {
      article.message = 'Article deleted';
    }, function(error) {
      console.log(error);
    });
    $scope.title = "";
    $scope.deleteId = "";
  }

  $scope.hide = function(article) {
    article.hide = true;
  }

  $scope.send = function(isValid) {
    if(isValid) {
      var data = {
        title: $scope.sightseeingArticle.title,
        body: $scope.sightseeingArticle.body
      };
      console.log(data);
      SightseeingProvider.save(data, function(response) {
        $scope.sightseeingArticle = {};
        $scope.responseMessage = 'Well done! Article saved.';
        $scope.class = 'success';
        console.log(response);
      }, function(error) {
        console.log(error);
        $scope.responseMessage = 'Oops! Something went wrong.';
        $scope.class = 'danger';
      })
    }
  }
}])
 angular.module('app.controllers')
.controller('SignUpController', ['$scope', '$http', 'AdminProvider', function($scope, $http, AdminProvider) {


  $scope.submitForm = function(isValid) {
    if ($scope.password == $scope.repeat && isValid) {
      const data = {
        email: $scope.email,
        password: $scope.password
      };

      AdminProvider.save({id: '1'}, data)
      .$promise.then(function(result) {
        console.log(result);
      }, function(error) {
        console.log(error);
      });

      $scope.email = "";
      $scope.password = "";
      $scope.repeat = "";
    } else
      console.log('Zle haslo, leszczu!');
    }
}]);
 angular.module('app.controllers')
.controller('TestController', ['$scope', 'TestService', function($scope, TestService) {
  //$scope.users = TestService.query();
}]);
 angular.module('app.services')
.factory('AboutMainProvider', ['$resource',
  function($resource) {
    return $resource('/admin/about', null, {'put': {method: 'put'}})
}])
 angular.module('app.services')
.factory('AboutTabOneProvider', ['$resource',
  function($resource) {
    return $resource('/admin/aboutTabOne', null, {'put': {method: 'put'}})
}])
 angular.module('app.services')
.factory('AboutTabThreeProvider', ['$resource',
  function($resource) {
    return $resource('/admin/aboutTabThree', null, {'put': {method: 'put'}})
}])
 angular.module('app.services')
.factory('AboutTabTwoProvider', [
  '$resource',
  function($resource) {
    return $resource('admin/aboutTabTwo', null, {'PUT': {method: 'put'}});
  }
])
 angular.module('app.services')
.factory('AdminProvider', ['$resource', function($resource) {
  return $resource('/admin/:id', {id: '@id'})
}]);

//- GRUNTFILE CONCAT I UGLIFY I WATCH PROVIDERS
 angular.module('app.services')
.factory('authInterceptor', [
  '$q',
  '$injector',
  '$location',
  function($q, $injector, $location)
  {
    return {
      request: function(config) {
        // console.log('1');
        // console.log(config);
        return config;
      },

      requestError: function(config) {
        // console.log('2');
        // console.log(config);
        return config;
      },

      response: function(res) {
        //console.log('3');
        return res;
      },

      responseError: function(res) {
        //console.log('4');
        return res;
      }
    }
}
//    {
//   var service = this;
//
//   service.success = function()
//
//   service.response = function(response) {
//     if(response.status == 401) {
//       $injector.get('$state').transitionTo('admin');
//       return $q.reject(response);
//     } else {
//       return response;
//     }
//   };
//   service.responseError = function(response) {
//     if (response.status == 401){
//       $injector.get('$state').transitionTo('admin');
//       return $q.reject(response);
//     }
//     return $q.reject(response);
//   };
//
])
 
 // zmienic w gruntfile i index.html sciezki z providers na services

angular.module('app.services')
.factory('catalogService', [
  '$resource',
  function($resource) {
    return $resource('/admin/catalog', null, {'put': {method: 'put'}})
  }
])
 angular.module('app.services')
.factory('ContactProvider', ['$resource', function($resource) {
  return $resource('/admin/contact', null, {'put': {method: 'put'}})
}])
 angular.module('app.services')
.factory('MainPageProvider', ['$resource', function($resource) {
  return $resource('/admin/mainPage', null, {'put': {method: 'put'}});
}])
 angular.module('app.services')
.service('MenuService', function() {
  var dzejson = [
    {
      name: "main page",
      link: "home"
    },
    {
      name: "about us",
      link: "about.tabone"
    },
    {
      name: "sightseeing",
      link: "sightseeing"
    },
    {
      name: "catalog",
      link: "catalog"
    },
    {
      name: "contact",
      link: "contact"
    },
    {
      "name": "CMS",
      "link": "admin"
    }
  ];
  this.getMenu = function() {
    return dzejson;
  };
});
 angular.module('app.services')
.service('mockupMenuService', function() {
  var menu = [
    {
      name: 'kieliszki',
      subcategories: [
        {
          name: 'monika'
        },
        {
          name: 'oxford'
        },
        {
          name: 'wilanow'
        },
        {
          name: 'windsor'
        }
      ]
    },
    {
      name: 'karafki',
      subcategories: [
        {
          name: 'karafki'
        },
        {
          name: 'karafki na wode'
        },
        {
          name: 'karafki na perfumy'
        }
      ]
    },
    {
      name: 'wazony',
      subcategories: [
        {
          name: 'prasowane'
        },
        {
          name: 'w ksztalcie kuli'
        },
        {
          name: 'prostokatne'
        },
        {
          name: 'wazon-ryba'
        },
        {
          name: 'wazon-rog'
        }
      ]
    },
    {
      name: 'dzbanki'
    }
  ];
  this.getMenu = function() {
    return menu;
  }
});
 angular.module('app.services')
.factory('PlaceholderService', ['$resource', function($resource) {
  return $resource('https://jsonplaceholder.typicode.com/posts', {id: '@id'})
}]);
 angular.module('app.services')
.factory('productService', [
  '$resource',
  function($resource) {
    return $resource('/admin/product', {id: '@id'}, {'put': {method: 'put'}})
  }
])
 angular.module('app.services')
.factory('SightseeingProvider', ['$resource', function($resource) {
  return $resource('/admin/sightseeing', null, {'put': {method: 'PUT'}});
}])
 angular.module('app.services')
.factory('TestService', ['$resource', function($resource) {
  return {kupa: "kupa"} // $resource('/post/:id', {id: '@_id'})
}]);
 // angular.module('app.directives')
// .directive('addSightseeingArticle', function() {
//   return {
//     restrict: 'E',
//     link: function(scope, elem) {
//       elem.on('submit', function() {
//          scope.$broadcast('form:submit');
//       });
//     }
//   };
// });
 // angular.module('app.directives')
// .directive('myDirective', function() {
//   return {
//     require: '^form',
//     link: function(scope, elem, attr, form) {
//       scope.$on('form:submit', function() {
//
//         scope.send(isValid);
//         form.$setPristine();
//       });
//     }
//   };
// });
