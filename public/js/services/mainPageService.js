angular.module('app.services')
.factory('MainPageProvider', ['$resource', function($resource) {
  return $resource('/admin/mainPage', null, {'put': {method: 'put'}});
}])
