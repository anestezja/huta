angular.module('app.services')
.factory('AboutTabTwoProvider', [
  '$resource',
  function($resource) {
    return $resource('admin/aboutTabTwo', null, {'PUT': {method: 'put'}});
  }
])
