angular.module('app.services')
.service('mockupMenuService', function() {
  var menu = [
    {
      name: 'kieliszki',
      subcategories: [
        {
          name: 'monika'
        },
        {
          name: 'oxford'
        },
        {
          name: 'wilanow'
        },
        {
          name: 'windsor'
        }
      ]
    },
    {
      name: 'karafki',
      subcategories: [
        {
          name: 'karafki'
        },
        {
          name: 'karafki na wode'
        },
        {
          name: 'karafki na perfumy'
        }
      ]
    },
    {
      name: 'wazony',
      subcategories: [
        {
          name: 'prasowane'
        },
        {
          name: 'w ksztalcie kuli'
        },
        {
          name: 'prostokatne'
        },
        {
          name: 'wazon-ryba'
        },
        {
          name: 'wazon-rog'
        }
      ]
    },
    {
      name: 'dzbanki'
    }
  ];
  this.getMenu = function() {
    return menu;
  }
});
