angular.module('app.services')
.factory('AdminProvider', ['$resource', function($resource) {
  return $resource('/admin/:id', {id: '@id'})
}]);

//- GRUNTFILE CONCAT I UGLIFY I WATCH PROVIDERS
