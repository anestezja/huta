angular.module('app.services')
.factory('AboutMainProvider', ['$resource',
  function($resource) {
    return $resource('/admin/about', null, {'put': {method: 'put'}})
}])
