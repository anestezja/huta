angular.module('app.services')
.factory('AboutTabThreeProvider', ['$resource',
  function($resource) {
    return $resource('/admin/aboutTabThree', null, {'put': {method: 'put'}})
}])
