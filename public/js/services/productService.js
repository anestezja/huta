angular.module('app.services')
.factory('productService', [
  '$resource',
  function($resource) {
    return $resource('/admin/product', {id: '@id'}, {'put': {method: 'put'}})
  }
])
