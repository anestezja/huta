angular.module('app.services')
.service('MenuService', function() {
  var dzejson = [
    {
      name: "main page",
      link: "home"
    },
    {
      name: "about us",
      link: "about.tabone"
    },
    {
      name: "sightseeing",
      link: "sightseeing"
    },
    {
      name: "catalog",
      link: "catalog"
    },
    {
      name: "contact",
      link: "contact"
    },
    {
      "name": "CMS",
      "link": "admin"
    }
  ];
  this.getMenu = function() {
    return dzejson;
  };
});
