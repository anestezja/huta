// zmienic w gruntfile i index.html sciezki z providers na services

angular.module('app.services')
.factory('catalogService', [
  '$resource',
  function($resource) {
    return $resource('/admin/catalog', null, {'put': {method: 'put'}})
  }
])
