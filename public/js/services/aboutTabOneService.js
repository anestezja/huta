angular.module('app.services')
.factory('AboutTabOneProvider', ['$resource',
  function($resource) {
    return $resource('/admin/aboutTabOne', null, {'put': {method: 'put'}})
}])
