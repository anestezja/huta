angular.module('app.services')
.factory('ContactProvider', ['$resource', function($resource) {
  return $resource('/admin/contact', null, {'put': {method: 'put'}})
}])
