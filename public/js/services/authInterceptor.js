angular.module('app.services')
.factory('authInterceptor', [
  '$q',
  '$injector',
  '$location',
  function($q, $injector, $location)
  {
    return {
      request: function(config) {
        // console.log('1');
        // console.log(config);
        return config;
      },

      requestError: function(config) {
        // console.log('2');
        // console.log(config);
        return config;
      },

      response: function(res) {
        //console.log('3');
        return res;
      },

      responseError: function(res) {
        //console.log('4');
        return res;
      }
    }
}
//    {
//   var service = this;
//
//   service.success = function()
//
//   service.response = function(response) {
//     if(response.status == 401) {
//       $injector.get('$state').transitionTo('admin');
//       return $q.reject(response);
//     } else {
//       return response;
//     }
//   };
//   service.responseError = function(response) {
//     if (response.status == 401){
//       $injector.get('$state').transitionTo('admin');
//       return $q.reject(response);
//     }
//     return $q.reject(response);
//   };
//
])
 
