angular.module('app.services')
.factory('SightseeingProvider', ['$resource', function($resource) {
  return $resource('/admin/sightseeing', null, {'put': {method: 'PUT'}});
}])
