angular.module('app.services')
.factory('PlaceholderService', ['$resource', function($resource) {
  return $resource('https://jsonplaceholder.typicode.com/posts', {id: '@id'})
}]);
