angular.module('app', [
  'ui.router',
  'ngResource',
  'ngCookies',
  'angularFileUpload',
  'app.controllers',
  'app.services',
  'app.directives'
])
.config([
  '$stateProvider',
  '$locationProvider',
  '$urlRouterProvider',
  '$httpProvider',

  function($stateProvider, $locationProvider, $urlRouterProvider, $httpProvider) {
  $locationProvider.html5Mode(true);
  $httpProvider.interceptors.push('authInterceptor');
  $stateProvider
    .state('home', {
      url: '/',
      views: {
        "": {
          templateUrl: 'templates/home.html',
          controller: "NewsController"
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('post', {
      url: '/post/:id',
      views: {
        "": {
          templateUrl: 'templates/home-news.html'
        },
        "carousel": {
          templateUrl: "templates/baner.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('about', {
      url: '/about',
      views: {
        "": {
          templateUrl: 'templates/about.html',
          controller: 'AboutMainController'
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('about.tabone', {
      url: '/tabone',
      templateUrl: 'templates/about-tabone.html',
      controller: 'AboutTabOneController'
    })
    .state('about.tabtwo', {
      url: '/tabtwo',
      templateUrl: 'templates/about-tabtwo.html',
      controller: 'AboutTabTwoController'
    })
    .state('about.tabthree', {
      url: '/tabthree',
      templateUrl: 'templates/about-tabthree.html',
      controller: 'AboutTabThreeController'
    })
    .state('sightseeing', {
      url: '/sightseeing',
      views: {
        "": {
          templateUrl: 'templates/sightseeing.html'
        },
        "carousel": {
          templateUrl: "templates/baner.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('catalog', {
      url: '/catalog',
      views: {
        "": {
          templateUrl: 'templates/catalog.html'
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        },
        'subcategories@catalog': {
          templateUrl: "templates/subcategories.html"
        },
        'products@catalog': {
          templateUrl: "templates/products.html"
        }
      }
    })
    .state('contact', {
      url: '/contact',
      views: {
        "": {
          templateUrl: 'templates/contact.html',
          controller: 'ContactController'
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('product', {
      url: '/product/:id',
      views: {
        "": {
          templateUrl: 'templates/product.html',
          controller: 'productCtrl'
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      },
      params: {
        id: null
      }
    })
    .state('admin', {
      url: '/admin',
      views: {
        "": {
          templateUrl: 'templates/admin-forms.html',
          controller: 'AdminFormsController'
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      },
      params: {
        message: ''
      }
    })
    .state('loggedIn', {
      url: '/logged-in',
      views: {
        "": {
          templateUrl: 'templates/logged-in.html',
          resolve: {
            exampleObject: function() {
              return {
                title: 'Zarządzanie treściami strony',
                body: 'Lorem ipsum dolor sit amet.'
              }
            }
          },
          controller: 'AdminController'
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('update', {
      url: '/update',
      views: {
        "": {
          templateUrl: 'templates/update.html',
          resolve: {
            responseObject: function($http) {
              return $http.get('/update').then(function(response) {
                console.log(response);
                response.noError = true;
                return response;
              }, function(error) {
                console.log(error);
                error.noError = false;
                return error;
              })
            }
          },
          controller: function($http, $scope, responseObject) {
            // return $http.get('/update').then(function(response) {
            //   console.log(response);
            // }, function(error) {
            //   console.log(error);
            // });
            console.log(responseObject);
            if (responseObject.noError == true) {
              $scope.response = responseObject;
              $scope.noError = responseObject.noError;
            } else {
              $state.go('admin', {message: 'Zaloguj się aby uzyskać dostęp do panelu.'});
            }
          },
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('loggedOut', {
      url: '/logged-out',
      views: {
        "": {
          templateUrl: 'templates/logged-out.html'
        },
        "carousel": {
          templateUrl: "templates/carousel.html"
        },
        "footer": {
          templateUrl: "templates/footer.html"
        }
      }
    })
    .state('loggedIn.sightseeing', {
      url: '/sightseeing',
      templateUrl: 'templates/cms/sightseeing1.html',
      controller: 'CMSsightseeingController'
    })
    .state('loggedIn.sightseeingArticle', {
      url: '/sightseeing/:id',
      templateUrl: 'templates/cms/sightseeing_article.html',
      params: {
        article: null
      },
      controller: 'CMSsightseeingArticleController'
    })
    .state('loggedIn.mainPage', {
      url: '/mainPage',
      //templateUrl: 'templates/cms/main_page.html',
      templateUrl: 'templates/cms/main_page_articles_list.html',
      controller: 'CMSmainPageArticlesListCtrl'
      //controller: 'CMSmainPageController'
    })
    .state('loggedIn.mainPageArticle', {
      url: '/mainPage/:id',
      params: {
        article: null
      },
      templateUrl: 'templates/cms/main_page_article.html',
      controller: 'CMSmainPageArticleCtrl'
    })

    /* -----
    // CMS ABOUT
    ----- */

    .state('loggedIn.about', {
      url: '/about',
      templateUrl: 'templates/cms/about.html',
      controller: 'CMSaboutCtrl'
    })
    .state('loggedIn.about.main', {
      url: '/main',
      templateUrl: 'templates/cms/about-main.html',
      controller: 'CMSaboutMainCtrl'
    })
    .state('loggedIn.about.tabone', {
      url: '/tabone',
      templateUrl: 'templates/cms/about-tab-one.html',
      controller: 'CMSaboutTabOneCtrl'
    })
    .state('loggedIn.about.tabtwo', {
      url: '/tabtwo',
      templateUrl: 'templates/cms/about-tab-two.html',
      controller: 'CMSaboutTabTwoCtrl'
    })
    .state('loggedIn.about.tabthree', {
      url: '/tabthree',
      templateUrl: 'templates/cms/about-tab-three.html',
      controller: 'CMSaboutTabThreeCtrl'
    })

    /* -----
    // END OF CMS ABOUT
    ----- */

    .state('loggedIn.catalog', {
      url: '/catalog',
      templateUrl: 'templates/cms/catalog.html',
      controller: 'CMScatalogCtrl'
    })
    .state('loggedIn.contact', {
      url: '/contact',
      templateUrl: 'templates/cms/contact.html',
      controller: 'CMScontactCtrl'
    })
    .state('loggedIn.product', {
      url: '/product/:id',
      templateUrl: 'templates/cms/product.html',
      controller: 'CMSproductCtrl',
      params: {
        id: null
      }
    })

    ;$urlRouterProvider.otherwise('/');
}]);
// service tworzy obiekt, wiec mozna sie do niego odwolywac bezposrednio, np
// w kontrolerze myService.chapajDzide();
// factory to funkcja, wiec trzeba stworzyc obiekt, ktory returnuje, np
// return {name: 'danuta'}
// oba definiuje nazwa i funkcja. Oba mozna injectowac.
