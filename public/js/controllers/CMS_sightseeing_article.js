angular.module('app.controllers')
.controller('CMSsightseeingArticleController', [
  '$scope',
  'SightseeingProvider',
  '$stateParams',
  'SightseeingProvider',
  function($scope, SightseeingProvider, $stateParams, SightseeingProvider) {
    $scope.title = $stateParams.article.title;
    $scope.body = $stateParams.article.body;
    $scope.showMessage = false;

    $scope.save = function(isValid) {
      if(!isValid) {
        $scope.showMessage = true;
        $scope.class = 'warning';
        $scope.responseMessage = 'Przed przesłaniem artykułu wprowadź w nim zmiany lub ';
      } else if(isValid) {
        var data = {
          id: $stateParams.article._id,
          title: $scope.title,
          body: $scope.body
        }
        //console.log(data);
        SightseeingProvider.put(data, function(response) {
          $scope.showMessage = true;
          $scope.class = 'success';
          $scope.responseMessage = 'Zapisano zmiany. ';
          console.log('response: ');
          console.log(response);
        }, function(error) {
          $scope.showMessage = true;
          $scope.class = 'danger';
          $scope.responseMessage = 'Wystąpił błąd. ';
          console.log(error);
        })
      }
    }
}])
