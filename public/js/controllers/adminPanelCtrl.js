angular.module('app.controllers')
.controller('AdminController', [
  '$scope',
  'exampleObject',
  'AdminProvider',
  '$state',
  '$stateParams',
  function($scope, exampleObject, AdminProvider, $state) {
  $scope.title = exampleObject.title;
  $scope.body = exampleObject.body;
  $scope.logout = function() {
    AdminProvider.get({id: 3}, function(result) {
      console.log(result);
      $state.go('admin', {message: 'Well done! You are succesfully logged out.'});
    }, function(error) {
      console.log(error);
      $state.go('admin');
    })
  }
  $scope.menu = [
    {
      name: 'main page contents',
      link: 'loggedIn.mainPage'
    },
    {
      name: 'about us',
      link: 'loggedIn.about'
    },
    {
      name: 'sightseeing',
      link: 'loggedIn.sightseeing'
    },
    {
      name: 'catalog',
      link: 'loggedIn.catalog'
    },
    {
      name: 'contact',
      link: 'loggedIn.contact'
    },


  ]
}]);
