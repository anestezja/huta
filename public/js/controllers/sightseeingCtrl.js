angular.module('app.controllers')
.controller('CMSsightseeingController', [
  '$scope',
  'SightseeingProvider',
  '$http',
  function($scope, SightseeingProvider, $http) {
  $scope.responseMessage = '';

  SightseeingProvider.query(function(response) {
    console.log(response)
    $scope.articles = response;
  }, function(error) {
    console.log(err);
  })

  $scope.launchModal = function(article) {
    $scope.title = article.title;
    $scope.articleToDelete = article;
  }

  $scope.delete = function(article) {
    data = {
      id: article._id
    };
    SightseeingProvider.delete(data, function(response) {
      article.message = 'Article deleted';
    }, function(error) {
      console.log(error);
    });
    $scope.title = "";
    $scope.deleteId = "";
  }

  $scope.hide = function(article) {
    article.hide = true;
  }

  $scope.send = function(isValid) {
    if(isValid) {
      var data = {
        title: $scope.sightseeingArticle.title,
        body: $scope.sightseeingArticle.body
      };
      console.log(data);
      SightseeingProvider.save(data, function(response) {
        $scope.sightseeingArticle = {};
        $scope.responseMessage = 'Well done! Article saved.';
        $scope.class = 'success';
        console.log(response);
      }, function(error) {
        console.log(error);
        $scope.responseMessage = 'Oops! Something went wrong.';
        $scope.class = 'danger';
      })
    }
  }
}])
