angular.module('app.controllers')
.controller('NewsController', [
  '$scope',
  '$http',
  '$stateParams',
  'TestService',
  'MainPageProvider',
  function($scope, $http, $stateParams, TestService, MainPageProvider) {
    MainPageProvider.query(function(response) {
      if(!response[0]) {
        console.log('brak wpisow');
      } else {
        $scope.articles = response;

        //masonry();
      }
    })

  //console.log($stateParams);


  //$scope.users = TestService.query();

  // $http.get('/news').then(function(response) {
  //   $scope.dupa = response.data;
  //   console.log(response.data);
  // }, function(response) {
  //   var status = response.status;
  //   console.log(status);
  // });

  $scope.sendNews = function() {
    var dataObject = {
                      title: $scope.title,
                      body: $scope.body
    };
    console.log("Sending object dataObject through angular $http:");
    console.log(dataObject);

    $http.post("/new", JSON.stringify(dataObject), {headers: {"Content-type": "application/json"}})
    .then(function(res) {
      //sukces
    });
  };
}]);
