angular.module('app.controllers')
.controller('AboutTabOneController', [
  '$scope', 'AboutTabOneProvider',
  function($scope, AboutTabOneProvider) {

    AboutTabOneProvider.query(function(response) {
      console.log(response);
      $scope.tabOneText = response[0];
    }, function(error) {
      console.log(error);
    });


  }
])
