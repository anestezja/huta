angular.module('app.controllers')
.controller('AboutTabTwoController', [
  '$scope', 'AboutTabTwoProvider',
  function($scope, AboutTabTwoProvider) {
    AboutTabTwoProvider.query(function(response) {
      $scope.events = response;
    }, function(error) {
      console.log(error);
    })
  }
])
