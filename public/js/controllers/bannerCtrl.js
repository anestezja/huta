angular.module('app.controllers')
.controller('BannerController', [function() {
  if(window.matchMedia('(max-width: 768px)').matches) {
    document.querySelector('.MainPageBaner').style.backgroundImage = "url(../img/nowe/slider/slider2mini-s.jpg)";
    //$scope.src = 'img/nowe/slider/slider2mini.jpg';
  } else {
    document.querySelector('.MainPageBaner').style.backgroundImage = "url(../img/nowe/slider/p19-widen-s-s.jpg)";
    //$scope.src = 'img/nowe/slider/slider2.jpg';
  }
}])
