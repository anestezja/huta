angular.module('app.controllers')
.controller('SignUpController', ['$scope', '$http', 'AdminProvider', function($scope, $http, AdminProvider) {


  $scope.submitForm = function(isValid) {
    if ($scope.password == $scope.repeat && isValid) {
      const data = {
        email: $scope.email,
        password: $scope.password
      };

      AdminProvider.save({id: '1'}, data)
      .$promise.then(function(result) {
        console.log(result);
      }, function(error) {
        console.log(error);
      });

      $scope.email = "";
      $scope.password = "";
      $scope.repeat = "";
    } else
      console.log('Zle haslo, leszczu!');
    }
}]);
