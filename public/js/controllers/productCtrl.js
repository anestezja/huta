angular.module('app.controllers')
.controller('productCtrl', [
  '$scope', 'productService', '$stateParams',
  function($scope, productService, $stateParams) {
    productService.get({id: $stateParams.id}, function(result) {
      if(result) {
        $scope.product = result;
      } else {
        console.log('brak wpisow');
      }
    });

  }
])
