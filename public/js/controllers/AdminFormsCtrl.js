angular.module('app.controllers')
.controller('AdminFormsController', [
  '$scope',
  '$http',
  '$state',
  'AdminProvider',
  '$cookies',
  '$stateParams',
function($scope, $http, $state, AdminProvider, $cookies, $stateParams) {
  $scope.loggedOutMessage = $stateParams.message;

  $scope.signIn = function(isValid) {
    if (isValid) {
      const data = {
        email: $scope.signInForm.email,
        password: $scope.signInForm.password
      };

      $scope.signInForm.emailMessage = "";
      $scope.signInForm.passMessage = "";

      AdminProvider.save({id: '2'}, data)
      .$promise.then(function(result) {

        console.log(result);
        if(result.id == 0 && result.result == true) { // udane logowanie
          const state = 'loggedIn';
          $state.go(state);
          console.log(document.cookie);
        } else if(result.id == 0 && result.result == false) { // bledne haslo
          $scope.signInForm.passMessage = 'Błędne haslo'
        } else if(result.id == -1) { // nie ma takiego maila w bazie
          $scope.signInForm.emailMessage = result.message;
        }
      }, function(error) {
          //console.log(error);
      });

    } else
      console.log('Cos nie tak.')
    }


    $scope.signUp = function(isValid) {
      if ($scope.signUpForm.password == $scope.signUpForm.repeat && isValid) {
        const data = {
          email: $scope.signUpForm.email,
          password: $scope.signUpForm.password
        };

        AdminProvider.save({id: '1'}, data)
        .$promise.then(function(result) {
          console.log(result);
        }, function(error) {
          console.log(error);
        });

        $scope.signUpForm.email = "";
        $scope.signUpForm.password = "";
        $scope.signUpForm.repeat = "";
      } else
        console.log('Zle haslo, leszczu!');
      }


}]);
