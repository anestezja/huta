
angular.module('app.controllers')
.controller('CMSaboutTabTwoCtrl', [
  '$scope', 'AboutTabTwoProvider',
  function($scope, AboutTabTwoProvider) {

    AboutTabTwoProvider.query(function(response) {
      $scope.events = response;
    }, function(error) {
      console.log(error);
    });

    $scope.send = function(valid) {
      if(valid) {
        let data = {
          date: new Date($scope.event.year, $scope.event.month - 1, $scope.event.date),
          body: $scope.event.body
        };
        console.log(data);
        $scope.event = {};
        AboutTabTwoProvider.save(data)
        .$promise.then(function(result) {
          console.log(result);
        }, function(error) {
          console.log(error);
        })
      }
    };


  // days array
  $scope.daysArray = [];
  for(var i = 1; i < 32; i++) {
    $scope.daysArray.push(i);
  };

  // months array
  $scope.monthsArray = [];
  for(var j = 1; j < 13; j++) {
    $scope.monthsArray.push(j);
  }

  $scope.delete = function(id) {

  };

  $scope.edit = function(event) {

  }

}])

// new Date(year, month, date, hours, minutes, seconds, ms)
// year with 4 digits
// months start with
// if 'date' absent, 1 assumed
// if hours / minutes / seconds / ms absent, 0 assumed
