angular.module('app.controllers')
.controller('CMSproductCtrl', [
  '$scope', '$stateParams', 'productService',
  function($scope, $stateParams, productService) {
    productService.get({id: $stateParams.id}, function(response) {
      if(!response) {
        console.log('brak wpisu');
      } else {
        $scope.product = response;
        console.log(response);
      }
    });


    $scope.editionMode = false;

    $scope.edit = function() {
      return $scope.editionMode = true;
    }

    $scope.cancel = function() {
      return $scope.editionMode = false;
    }

    $scope.save = function(valid) {
      if(valid) {
        let data = $scope.product;

        productService.put(data, function(response) {
          if(!response) {
            console.log('blad');
          } else {
            console.log('zajebongo');
          }
        })
      }
    };

    $scope.delete = function() {
      productService.delete({id: $scope.product._id}, function(response) {
        if(!response) {
          console.log('blad');
        } else {
          console.log('zajebongo');
        }
      })
    }


  }
])
