angular.module('app.controllers')
.controller('CMScontactCtrl', [
  '$scope', 'ContactProvider',
  function($scope, ContactProvider) {
    ContactProvider.query(function(response) {
      $scope.contacts = response;
      console.log(response);
    }, function(error) {
      console.log(error);
    })

    $scope.send = function(valid) {
      if(valid) {
        let data = {
          title: $scope.addContact.title,
          body: $scope.addContact.body
        }
        $scope.addContact = {};
        ContactProvider.save(data)
        .$promise.then(function(result) {
          console.log(result);
        }, function(error) {
          console.log(error);
        })
      }
    }

    $scope.edit = function(index) {
      return $scope.editable = index;
    }

    $scope.saveChanges = function(contact) {
      // jeszcze walidacja, po to byla nazwa formularza, kurwa
      // reczna walidacja
      let data = {
        id: contact._id,
        title: contact.title,
        body: contact.body
      };
      let valid = {};
      if(valid) {
        console.log('chuj chuj');
        ContactProvider.put(data, function(response) {
          console.log(response);
        }, function(error) {
          console.log(error);
        })
      }
    }

    // TO DO: przypomnienie o zapisaniu zmian
    // ale to moze dopiero przy wysiwyg'u
    // $scope.savingReminder = function(form) {
    //   console.log(form);
    // }

    $scope.remove = function(id) {
      let data = {
        id: id
      };
      ContactProvider.delete(data, function(response) {
        console.log(response);
      }, function(error) {
        console.log(error);
      })
    }

    $scope.cancel = function() {
      $scope.editable = null;
    }
  }
])
