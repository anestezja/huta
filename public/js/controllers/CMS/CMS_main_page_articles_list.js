angular.module('app.controllers')
.controller('CMSmainPageArticlesListCtrl', [
  '$scope',
  'MainPageProvider',
  function($scope, MainPageProvider) {
    MainPageProvider.query(function(response) {
      console.log(response);
      $scope.articles = response;
    }, function(error) {
      console.log(error)
    })

    $scope.send = function(isVaild) {
      var data = {
        title: $scope.mainPageArticle.title,
        body: $scope.mainPageArticle.body,
        image: $scope.mainPageArticle.image
      };
      console.log(data);
      MainPageProvider.save(data, function(response) {
        $scope.data = {};
        $scope.responseMessage = 'Well done! Article saved.';
        $scope.class = 'success';
      }, function(error) {
        console.log(error);
        $scope.responseMessage = 'Oops! Something went wrong.';
        $scope.class = 'danger';
      })
      $scope.mainPageArticle = {};
    };
    $scope.launchModal = function(article) {
      console.log('chuj');
    }
}])
