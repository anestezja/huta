angular.module('app.controllers')
.controller('CMScatalogCtrl', [
  '$scope', 'catalogService',
  function($scope, catalogService) {

  catalogService.query(function(response) {
    if(!response[0]) {
      console.log('brak wpisow');
    } else {
      console.log(response);
      $scope.products = response;
    }
  }, function(error) {
    console.log(error);
  });


  $scope.submit = function(valid) {
    if(valid) {
      let data = $scope.item;
      console.log(data);
      catalogService.save(data, function(response) {
        console.log(response);
        $scope.item = {}
      });
    }
  }

  }
])
