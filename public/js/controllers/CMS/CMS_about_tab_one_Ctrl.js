angular.module('app.controllers')
.controller('CMSaboutTabOneCtrl', [
  '$scope', 'AboutTabOneProvider',
  function($scope, AboutTabOneProvider) {

    AboutTabOneProvider.query(function(response) {
      if(!response[0]) {
        $scope.tabOneText = {};
        $scope.visibility = true;
      } else {
        $scope.tabOneText = response[0];
        $scope.visibility = false;
      }

    }, function(error) {
      console.log(error);
    });

    $scope.save = function(valid) {
      if(valid) {
        let data = {
          text: $scope.tabOneText.text
        };

        AboutTabOneProvider.save(data)
        .$promise.then(function(result) {
          console.log(result);
        }, function(error) {
          console.log(error);
        })
      }
    };

    $scope.update = function(valid, id) {
      console.log('id: ');
      console.log(id);
      if(valid) {
        let data = {
          id: id,
          text: $scope.tabOneText.text
        }

        AboutTabOneProvider.put(data)
        .$promise.then(function(result) {
          console.log(result);
        }, function(error) {
          console.log(error);
        })
      }
    };

  }
])
