angular.module('app')
.controller('CMSmainPageArticleCtrl', ['$scope', '$stateParams', function($scope, $stateParams) {
  $scope.title = $stateParams.article.title;
  $scope.body = $stateParams.article.body;
  $scope.image = $stateParams.article.image;
  $scope.showMessage = false;
  $scope.class = 'success';
}])
