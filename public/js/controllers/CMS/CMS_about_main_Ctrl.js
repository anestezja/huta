angular.module('app.controllers')
.controller('CMSaboutMainCtrl', [
  '$scope', 'AboutMainProvider',
  function($scope, AboutMainProvider) {

    AboutMainProvider.query(function(response) {
      console.log(response);
      if(!response[0]) {
        $scope.mainText = {};
        $scope.visibility = true;
      } else {
        $scope.mainText = response[0];
        $scope.visibility = false;
      }

    }, function(error) {
      console.log(error);
    })

    $scope.save = function(valid) {

        let data = {
          text: $scope.mainText.text
        };

        AboutMainProvider.save(data)
        .$promise.then(function(result) {
          console.log(result);
        }, function(error) {
          console.log(error);
        })
    }

    $scope.update = function(valid, id) {
      console.log('id: ');
      console.log(id);
      if(valid) {
        let data = {
          id: id,
          text: $scope.mainText.text
        }

        AboutMainProvider.put(data)
        .$promise.then(function(result) {
          console.log(result);
        }, function(error) {
          console.log(error);
        })
      }
    };


  }
])
