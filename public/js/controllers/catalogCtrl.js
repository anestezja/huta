angular.module('app.controllers')
.controller('CatalogController', [
  '$scope',
  '$http',
  'mockupMenuService',
  'catalogService',
  function($scope, $http, mockupMenuService, catalogService) {

  catalogService.query(function(response) {
    if(!response[0]) {
      console.log('brak wynikow');
    } else {
      $scope.products = response;
    }
  })

  /* --- menu i jakies glupoty --- */

  $scope.menu = mockupMenuService.getMenu();
  $scope.activeItem = [];
  $scope.activeSubItem = [];

  $scope.setMenu = function(x) {
    $scope.submenu = x.subcategories;
    $scope.activeItem = x;
    $scope.buttonText = 'kategoria: ' + x.name;
  };
  $scope.setSubmenu = function(x) {
    $scope.activeSubItem = x;
  }

  $scope.buttonText = 'kategorie';

  $scope.open = false;
  $scope.showSubcat = function() {
    if (window.matchMedia("(max-width: 768px)").matches) {
          ($scope.open === false)? $scope.open = true : $scope.open = false;
    }
  }

  /* --- koniec menu i glupot --- */

}]);
