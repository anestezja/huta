angular.module('app.controllers')
.controller('CMSmainPageController', ['$scope', 'FileUploader', function($scope, FileUploader) {
  var uploader = $scope.uploader = new FileUploader({
    url: '/admin/mainPage/file'
  });

  uploader.onCompleteAll = function() {
      console.info('onCompleteAll');
  };

  uploader.onCompleteItem = function(fileItem, response, status, headers) {
      console.info('onCompleteItem', fileItem, response, status, headers);
  };

  uploader.onSuccessItem = function(fileItem, response, status, headers) {
      console.info('onSuccessItem', fileItem, response, status, headers);
  };

  uploader.onErrorItem = function(fileItem, response, status, headers) {
      console.info('onErrorItem', fileItem, response, status, headers);
  };
  
  uploader.onCancelItem = function(fileItem, response, status, headers) {
      console.info('onCancelItem', fileItem, response, status, headers);
  };

}])
