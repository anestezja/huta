angular.module('app.controllers')
.controller('AboutTabThreeController', [
  '$scope', 'AboutTabThreeProvider',
  function($scope, AboutTabThreeProvider) {
    AboutTabThreeProvider.query(function(response) {
      $scope.events = response;
    }, function(error) {
      console.log(error);
    })
  }
])
