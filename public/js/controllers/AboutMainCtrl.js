angular.module('app.controllers')
.controller('AboutMainController', [
  '$scope', 'AboutMainProvider',
  function($scope, AboutMainProvider) {
    AboutMainProvider.query(function(response) {
      console.log(response);
      $scope.mainText = response[0];
    }, function(error) {
      console.log(error);
    })
  }
])
