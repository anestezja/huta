var express = require('express'),
    http = require('http'),
    path = require('path'),
    ecstatic = require('ecstatic'),
    bodyParser = require('body-parser'),
    nodemailer = require('nodemailer'),
    smtpTransport = require('nodemailer-smtp-transport'),
    ObjectId = require('mongodb').ObjectID,
    assert = require('assert'),
    bcrypt = require('bcrypt'),
    cookieParser = require('cookie-parser'),
    MongoClient = require('mongodb').MongoClient,
    session = require('express-session'),
    fileUpload = require('express-fileupload');


var app = express();
//var CMSsightseeingRoutes = require('./routes/CMSsightseeing.js');

app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({
  secret: 'keyboard cat',
  cookie: {
    maxAge: (60 * 1000 * 3)
  },
  saveUninitialized: false,
  resave: true
}));
app.use(fileUpload());
app.use(express.static(path.join(__dirname, 'public')));

//app.use('/admin/sightseeing', CMSsightseeingRoutes);

var db;
MongoClient.connect('mongodb://huta:huta123@ds127321.mlab.com:27321/huta', function(err, database) {
  if (err) return console.log(err);
  db = database;
  app.set('port', process.env.PORT || 8082);

  app.use(ecstatic({ root: __dirname + '/public' }));
  http.createServer(app).listen(app.get('port'), function () {
    console.log('App listening on port ' + app.get('port') + '!');
  });
})
app.get('', function(req, res) {
  res.sendFile(path.join(__dirname + '/public/templates/index.html'));
});
app.get('/home', function(req, res) {
  res.send(dzejson);
});
app.get('/news', function(req, res) {
  var cursor = db.collection('news').find().toArray(function(err, results) {
    res.send(results);
  })
});
app.post('/new', function(req, res) {
  console.log("First thing after getting the object:");
  console.log(req.body);
  db.collection('news').save(req.body, function(err, results) {
    if (err) {
      console.log(err);
    } else {
      console.log("saved to database");
    }
    res.redirect('/news');
  })
});
app.post('/send', function(req, res) {
  var mailOpts, smtpTrans;
  smtpTrans = nodemailer.createTransport(smtpTransport({
    service: 'Hotmail',
    auth:{
        user: "hutaszkla@hotmail.com",
        pass: "zawiercie123"
    },
    tls: {
        rejectUnauthorized: false
    }
  }));

  mailOpts = {
    from: 'hutaszkla@hotmail.com',
    to: 'hutaszkla@hotmail.com',
    subject: "Od: " + req.body.name + ", " + req.body.title,
    text: req.body.body + " Nadawca: " + req.body.email
  };

  smtpTrans.sendMail(mailOpts, function (error, response) {
    if (error) { // Email not sent
      res.send({ msg: 'Error occured, message not sent.' })
    }
    else { // Email sent
      res.send({ msg: 'Message sent! Thank you.' })
    }
  });

});
app.get('/post', function(req, res) {
  var cursor =
  db.collection('news').find().toArray(function(err, results) {
    res.send(results);
  })
});
app.get('/post/:id', function(req, res) {
  var cursor =
  db.collection('news').find({_id: ObjectId(req.params.id)}).toArray(function(err, results) {
    res.json(results);
  })

  // db.collection('news').find({_id: ObjectId(req.params.id)}, {}, function(err, results) {
  //   res.send(results);
  //   console.log(results);
  //   console.log(err);
  // })
});
app.post('/admin/1', function(req, res) {

  const saltRounds = 10;
  const myPlaintextPassword = req.body.password

  bcrypt.genSalt(saltRounds, function(err, salt) {
      bcrypt.hash(myPlaintextPassword, salt, function(err, hash) {
          // Store hash in your password DB.
          db.collection('admin').insertOne({
              email: req.body.email,
              password: hash
            })
            .then(function(result) {
              if (result) {
                res.send('Ok');
              } else {
                res.send('Blad.');
              }
          })
      });
  });
});

app.post('/admin/2', function(req, res) {

  function Response(id, message, data, status, result, email) {
    this.id = id;
    this.message = message;
    this.data = data;
    this.status = status;
    this.result = result;
    this.email = email;
  }

  db.collection('admin').find({
      email: req.body.email
    }).toArray()
    .then(function(data) {
      var hash = data[0].password;
      var myPlaintextPassword = req.body.password;
      bcrypt.compare(myPlaintextPassword, hash, function(err, result) {
          if (!err && result == true) {
            req.session.regenerate(function() {
              req.session.user = data[0].email;
              console.log(req.session);
              const noErrResp = new Response(0, 0, 0, 0, result, req.body.email)
              res.send(noErrResp);
            });
          } else if (!err && result == false) {
            const noErrResp = new Response(0, 0, 0, 0, result, req.body.email)
            res.send(noErrResp);
          } else {
            const errResp = new Response(0, "Jakis blad z odczytywania hasha")
            res.send(errResp);
          }
      });
    })
    .catch(function(err) {
      const errResp = new Response(-1, 'Użytkownik o podanym adresie email nie istnieje', err.data, err.status)
      res.send(errResp);
    });

});
app.get('/admin/3', function(req, res) {
  req.session.destroy(function() {
    return res.status(200).send({result: 'User logged out'});
  });

});
app.get('/update', sessionCheck, function(req, res) {
  res.send({result: req.session});
});

app.post('/admin/sightseeing', function (req, res) {
  console.log(req.body);
  db.collection('sightseeingArticles').insertOne({
    title: req.body.title,
    body: req.body.body
  })
  .then(function(result) {
    res.send(result);
  })
});
app.get('/admin/sightseeing', function(req, res) {
  var cursor = db.collection('sightseeingArticles').find().toArray(function(err, items) {
    if(!err) {
      res.send(items);
      //console.log(items);
    } else {
      res.send('nothing found');
    }
  });
});
app.delete('/admin/sightseeing', function(req, res) {
  console.log(req.query);
  db.collection('sightseeingArticles').remove({_id: new ObjectId(req.query.id)}, function(err, WriteResult) {
    if(!err) {
      console.log(WriteResult);
      res.send(WriteResult);
    } else {
      console.log('error');
    }
  });
});
app.put('/admin/sightseeing', function (req, res) {
  console.log('req.body: ');
  console.log(req.body);
  db.collection('sightseeingArticles').update(
    {
      _id: new ObjectId(req.body.id)
    },
    {
      title: req.body.title,
      body: req.body.body
    })
  .then(function(result) {
    res.send(result);
  })
});

app.post('/admin/mainPage/file', function (req, res) {
  console.log('req: ');
  console.log(req.files);
  var sampleFile = req.files.file;
  var path = './uploaded-files/' + req.files.file.name;
  console.log(path);
  sampleFile.mv(path, function(err) {
    if (err) {
      return res.status(500).send(err);
    } else {
      res.send('superowo, bajer, Danuta');
    }
  });

  // db.collection('sightseeingArticles').update(
  //   {
  //     _id: new ObjectId(req.body.id)
  //   },
  //   {
  //     title: req.body.title,
  //     body: req.body.body
  //   })
  // .then(function(result) {
  //   res.send(result);
  // })
  //res.send('dupa');
});
app.get('/admin/mainPage', function(req, res) {
  var cursor = db.collection('mainPageArticles').find().toArray(function(err, items) {
    if(!err) {
      res.send(items);
      //console.log(items);
    } else {
      res.send('nothing found');
    }
  });
});
app.post('/admin/mainPage', function(req, res) {
  console.log(req.body);
  db.collection('mainPageArticles').insertOne({
    title: req.body.title,
    body: req.body.body,
    image: req.body.image
  })
  .then(function(result) {
    res.send(result);
  })
});
app.post('/admin/contact', function(req, res) {
  console.log(req.body);
  db.collection('contacts').insertOne({
    title: req.body.title,
    body: req.body.body
  })
  .then(function(result) {
    res.send(result);
  })
});
app.get('/admin/contact', function(req, res) {
  let cursor = db.collection('contacts').find().toArray(function(err, items) {
    if(!err) {
      res.send(items); // jak niczego nie znajdzie, wysyla pusta tablice
    } else {
      res.send('error');
    }
  })
});
app.put('/admin/contact', function(req, res) {
  console.log("req.body: ");
  console.log(req.body);
  db.collection('contacts').update(
    {
      _id: new ObjectId(req.body.id)
    },
    {
      title: req.body.title,
      body: req.body.body
    })
  .then(function(result) {
    res.send(result);
  })
});
app.delete('/admin/contact', function(req, res) {
  console.log(req.query.id);
  db.collection('contacts').remove({_id: new ObjectId(req.query.id)}, function(err, WriteResult) {
    if(!err) {
      res.send(WriteResult);
    } else {
      console.log('error');
    }
  });
});
app.get('/admin/about', function(req, res) {
  // res.send({dupa: 'dupa'});
  db.collection('aboutTexts').find().toArray(function(err, items) {
    if(!err) {
      res.send(items);
    } else {
      res.send('error');
    }
  })
});
app.post('/admin/about', function(req, res) {
  console.log(req.body);
  db.collection('aboutTexts').insertOne({
    text: req.body.text
  })
  .then(function(result) {
    res.send(result)
  })
})
app.put('/admin/about', function(req, res) {
  console.log(req.body);
  db.collection('aboutTexts').update(
    {
      _id: new ObjectId(req.body.id)
    },
    {
      $set : {
                text: req.body.text
              }
    }
  )
  .then(function(result) {
    res.send(result)
  })
})
app.get('/admin/aboutTabTwo', function(req, res) {
  db.collection('aboutEvents').find().toArray(function(err, items) {
    if(!err) {
      res.send(items);
    } else {
      res.send('error');
    }
  })
});
app.post('/admin/aboutTabTwo', function(req, res) {
  console.log(req.body);
  db.collection('aboutEvents').insertOne({
    date: req.body.date,
    body: req.body.body
  })
  .then(function(result) {
    res.send(result)
  })
});
app.post('/admin/aboutTabOne', function(req, res) {
  db.collection('aboutTabOne').insertOne({
    text: req.body.text
  })
  .then(function(result) {
    res.send(result)
  })
});
app.get('/admin/aboutTabOne', function(req, res) {
  db.collection('aboutTabOne').find().toArray(function(err, items) {
    if(!err) {
      res.send(items);
    } else {
      res.send(err);
    }
  })
});
app.put('/admin/aboutTabOne', function(req, res) {
  db.collection('aboutTabOne').updateOne(
    {
      _id: new ObjectId(req.body.id)
    },
    {
      $set: {
        text: req.body.text
      }
    }
  ).then(function(result) {
    res.send(result);
  })
});
app.post('/admin/catalog', function(req, res) {
  db.collection('catalog').insertOne({
    name: req.body.name,
    text: req.body.secondaryText,
    weight: req.body.weight,
    symbol: req.body.symbol,
    description: req.body.description
  }).then(function(result) {
    res.send(result)
  })
});
app.get('/admin/catalog', function(req, res) {
  db.collection('catalog').find().toArray(function(err, items) {
    if(!err) {
      res.send(items)
    } else {
      res.send(err)
    }
  })
});
app.get('/admin/product', function(req, res) {
  console.log(req.query.id);
  db.collection('catalog').findOne(ObjectId(req.query.id), function(err, result) {
    if(!err) {
      console.log(result);
      res.send(result);
    }
  });
  //res.send(data);
});
app.put('/admin/product', function(req, res) {
  console.log(req.body);
  db.collection('catalog').updateOne(
    {
      _id: new ObjectId(req.body._id)
    },
    {
      $set: {
        name: req.body.name,
        text: req.body.text,
        weight: req.body.weight,
        symbol: req.body.symbol,
        description: req.body.description
      }
    }
  ).then(function(result) {
    res.send(result);
  })
});
app.delete('/admin/product', function(req, res) {
  console.log('dupa dupa dupa ======================');
  console.log(req.query.id);
  db.collection('catalog').remove({_id: new ObjectId(req.query.id)}, function(err, WriteResult) {
    if(!err) {
      res.send(WriteResult);
    } else {
      console.log('error');
    }
  });
})

app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname + '/public/templates/index.html'));
});
//* --- funkcja autoryzacji --- *//
function sessionCheck(req, res, next){
    if(req.session.user) {
      next();
    } else {
      console.log('before');
      //return res.redirect('/admin');
      return res.status(200).send('Unauthorized');
      //console.log('after');
    }
}

module.exports = app;
